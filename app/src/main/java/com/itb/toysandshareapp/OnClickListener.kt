package com.itb.toysandshareapp

import com.itb.toysandshareapp.model.Product

interface OnClickListener {
    fun onClick(product: Product)
}