package com.itb.toysandshareapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.itb.toysandshareapp.databinding.ActivityLoginBinding
import com.itb.toysandshareapp.databinding.FragmentLoginBinding

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}