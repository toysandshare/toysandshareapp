package com.itb.toysandshareapp.retrofit

import com.google.gson.GsonBuilder
import com.itb.toysandshareapp.model.*
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface API {
    //Aquí posem les operacions GET,POST, PUT i DELETE vistes abans
    @GET("products")
    suspend  fun getLists() : Response<MutableList<Product>>

    @GET("products/{id}")
    fun getProduct(@Path("id") id : Int) : Call<Product>
    @DELETE("products/{id}") fun removeList(@Path("id") id : Int) : Call<Int>
    @PUT("users/{id}/products")     fun updateProductWithUserId(@Path("id") id : Int, @Body product : Product) : Call<Product>
    @POST("users/{id}/products")    fun addProductWithUserId(@Path("id") id : Int, @Body product : Product) : Call<Product>

    @GET("userDetail")
    fun getUser(@Header("Authorization") authHeader: String) : Call <Usuari>


    @GET("users/{id}")    fun getUsuari(@Path("id") id : Int) : Call<Usuari>
    @POST("users")        fun addUsuari(@Body user : Register) : retrofit2.Call<ResponseBody>
    @PUT("users")         fun updateUsuari(@Body user : Usuari) : Call<Usuari>
    @DELETE("users/{id}") fun deleteUsuari(@Path("id")id : Int) : Call<Int>

    class BasicAuthInterceptor(username: String, password: String): Interceptor {
        private var credentials: String = Credentials.basic(username, password)

        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            var request = chain.request()
            request = request.newBuilder().header("Authorization", credentials).build()
            return chain.proceed(request)
        }
    }

    companion object {
        // 10.0.2.2 és el localhost para el emulador sino no funcionarà
//        val BASE_URL = "http://10.0.2.2:8080/"
        val BASE_URL = "https://toysandshare.herokuapp.com/"
        fun create(email: String, password: String): API {
            val interceptor = BasicAuthInterceptor(email, password)
            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(API::class.java)
        }

        fun getRetrofitInstance(): Retrofit {
            val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            }

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

}
