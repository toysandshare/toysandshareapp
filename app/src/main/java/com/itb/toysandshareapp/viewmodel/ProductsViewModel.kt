package com.itb.toysandshareapp.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itb.toysandshareapp.model.Product
import com.itb.toysandshareapp.model.Usuari
import com.itb.toysandshareapp.retrofit.API
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductsViewModel : ViewModel() {

    var userLogged = MutableLiveData<Usuari>()
    lateinit var api: API
    val products = MutableLiveData<MutableList<Product>>()
    var selectedProduct = MutableLiveData<Product>()

    var userVendor = MutableLiveData<Usuari>()

    fun connect(email:String, password: String){
        api = API.create(email, password)
        fetchProductes()
    }

    fun connectMyInfo(email:String, password: String){
        api = API.create(email, password)
        myInfo(email)
    }

    private fun fetchProductes() {
        viewModelScope.launch {
            val response = api.getLists()
            products.value = response.body()!!

        }

    }

    fun select(product: Product){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { api.getProduct(product.id!!) }
            selectedProduct.postValue(product)
        }
    }

    fun add(idUser: Int, product: Product){
        val call = api.addProductWithUserId(idUser, product)
            call.enqueue(object : Callback<Product>{
            override fun onResponse(call: Call<Product>, response: Response<Product>) {
                addView(product)
            }

            override fun onFailure(call: Call<Product>, t: Throwable) {
                Log.e("INSERT", "Error al insertar un producto")
            }
        })
    }

    fun update(id : Int, product : Product) {
        val call = api.updateProductWithUserId(id, product)
        call.enqueue(object : Callback<Product> {
            override fun onResponse(call: Call<Product>, response: Response<Product>) {
                Log.i("Product updated", product.toString())
            }
            override fun onFailure(call: Call<Product>, t: Throwable) {
                Log.i("Product not updated", product.toString())
            }
        })
    }

    fun remove(product : Product) {
        val call = product.id?.let { api.removeList(it) }
        if (call != null) {
            call.enqueue(object : Callback<Int> {
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    deleteView(product)
                }
                override fun onFailure(call: Call<Int>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        }
    }

    fun removeUser(usuari: Usuari) {
        val call = usuari.id.let { api.deleteUsuari(it) }
        call.enqueue(object : Callback<Int> {
            override fun onResponse(call: Call<Int>, response: Response<Int>) {
            }
            override fun onFailure(call: Call<Int>, t: Throwable) {
            }
        })

    }

    fun updateUser(usuari : Usuari) {
        val call = api.updateUsuari(usuari)
        call.enqueue(object: Callback<Usuari> {
            override fun onResponse(call: Call<Usuari>, response: Response<Usuari>) {
                userLogged.value = response.body()
            }
            override fun onFailure(call: Call<Usuari>, t: Throwable) {
            }
        })
    }

    fun myInfo(email: String) {
        val call = api.getUser(email)
        call.enqueue(object: Callback<Usuari> {
            override fun onResponse(call: Call<Usuari>, response: Response<Usuari>) {
                userLogged.value = response.body()!!
            }
            override fun onFailure(call: Call<Usuari>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun userInfo(id: Int){
        val call = api.getUsuari(id)
        call.enqueue(object: Callback<Usuari> {
            override fun onResponse(call: Call<Usuari>, response: Response<Usuari>) {
                userVendor.value = response.body()
            }
            override fun onFailure(call: Call<Usuari>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun addView(product : Product) {
        products.value!!.add(product)
    }
    fun deleteView(product : Product) {
        products.value!!.remove(product)
    }
    fun selectUser(usuari: Usuari){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { api.getUsuari(usuari.id!!) }
            userLogged.postValue(usuari)
        }
    }

}