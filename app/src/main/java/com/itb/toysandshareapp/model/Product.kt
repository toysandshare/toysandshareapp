package com.itb.toysandshareapp.model

import java.time.ZonedDateTime
import java.util.*

data class Product(
//    var dateCreated: Date,
    val id: Int? = 0,
    var imageLink: String,
    var price: Double,
    var productDescription: String,
    var productLocation: String,
    var productName: String,
//    var usuari: Usuari?,
    var usuari_id: Int

)