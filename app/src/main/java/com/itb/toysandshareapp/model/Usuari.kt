package com.itb.toysandshareapp.model

data class Usuari(
    var address: String,
    var city: String,
    var country: String,
    val dateCreated: String,
    val description: String,
    val email: String,
    val id: Int,
    val lastLogin: Int,
    var lastname: String,
    var name: String,
    val password: String,
    var postalCode: Int,
//    val products: MutableList<Product>,
    var profileImage: String,
    val phone: String,
    val status: Int
)