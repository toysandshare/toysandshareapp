package com.itb.toysandshareapp.model

data class Register (val name: String, val email: String, val password: String, val address: String, val city: String,
                     val country: String, val description: String, val lastname: String, val postalCode: Int, val phone: String )