package com.itb.toysandshareapp.model

data class LoginResponse (val id :Int = 0,
                            val adress: String = "",
                          val city: String = "",
                          val country: String = "",
                          val dateCreated: String = "",
                          val email: String = "",
                          val lastname: String = "",
                          val name: String = "",
                          val postalCode: Int = 0,
                          val profileImage: String = "")