package com.itb.toysandshareapp.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.storage.FirebaseStorage
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentEditProductBinding
import com.itb.toysandshareapp.model.Product
import com.itb.toysandshareapp.viewmodel.ProductsViewModel
import java.text.SimpleDateFormat
import java.util.*

class EditProductFragment : Fragment() {
    private val viewmodel : ProductsViewModel by activityViewModels()
    lateinit var binding: FragmentEditProductBinding
    private lateinit var productToEdit: Product

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentEditProductBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewmodel.selectedProduct.observe(viewLifecycleOwner) {
            val product = it
            binding.editProductName.editText?.text = product.productName.toEditable()
            binding.editProductDescription.editText?.text = product.productDescription.toEditable()
            binding.editProductAddress.editText?.text = product.productLocation.toEditable()
            binding.editProductPrice.editText?.text = "${product.price} €".toEditable()
            productToEdit = product
        }

        if (arguments?.getString("imageURI")!=null){
            Log.i("GettingImageOnFragment", arguments?.getString("imageURI")!!)
            binding.editProductImagePrev.setImageURI(arguments?.getString("imageURI")!!.toUri())
        }

        binding.addImages.setOnClickListener {
            findNavController().navigate(R.id.cameraFragment)
        }

        binding.updateProduct.setOnClickListener {
            if (arguments?.getString("imageURI").equals(null) || arguments?.getString("imageURI").equals("") || arguments?.getString("imageURI").equals(" ")){
                Toast.makeText(context, "You forgot the image!", Toast.LENGTH_SHORT).show()
            }else{
                addPhotoAndProduct()
            }
        }
    }

    private fun addPhotoAndProduct() {
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        val fileName = formatter.format(now)
        val storage = FirebaseStorage.getInstance().getReference("images/$fileName")

        storage.putFile(arguments?.getString("imageURI")!!.toUri())
            .addOnSuccessListener {
                binding.editProductImagePrev.setImageURI(null)
                Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                productToEdit.productName = binding.editProductName.editText?.text.toString().trim()
                productToEdit.price = binding.editProductPrice.editText?.text.toString().toDouble()
                productToEdit.productDescription = binding.editProductDescription.editText?.text.toString().trim()
                productToEdit.productLocation = binding.editProductAddress.editText?.text.toString().trim()
                productToEdit.imageLink = fileName
                viewmodel.update(viewmodel.userLogged.value!!.id, productToEdit)
                findNavController().navigate(R.id.nav_list)
            }.addOnFailureListener {
                Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
            }
    }

    private fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

}