package com.itb.toysandshareapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.navigation.fragment.findNavController
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentRegisterBinding
import com.itb.toysandshareapp.model.Register
import com.itb.toysandshareapp.retrofit.API
import retrofit2.Call
import retrofit2.Response

class RegisterFragment : Fragment() {
    lateinit var binding: FragmentRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.Bregister.setOnClickListener {

            if (binding.password.editText?.text.toString().trim() == binding.password2.editText?.text.toString().trim()) {
                val name = binding.name.editText?.text.toString().trim()
                val correo = binding.email.editText?.text.toString().trim()
                val password = binding.password.editText?.text.toString().trim()
                val address = binding.adress.editText?.text.toString().trim()
                val city = binding.city.editText?.text.toString().trim()
                val country = binding.country.editText?.text.toString().trim()
                val description = binding.description.editText?.text.toString().trim()
                val lastname = binding.lastname.editText?.text.toString().trim()
                val postalCode = binding.postalCode.editText?.text.toString().trim()
                val phone = binding.telefono.editText?.text.toString().trim()
                singUp(name, correo , password, address, city, country, description, lastname, postalCode, phone)
            } else{
                Toast.makeText(requireContext(), "Las contraseñas no son iguales intentalo de nuevo.", Toast.LENGTH_SHORT)
                    .show()            }
        }
        
    }

    private fun singUp(
        name: String,
        correo: String,
        password: String,
        address: String,
        city: String,
        country: String,
        description: String,
        lastname: String,
        postalCode: String,
        phone: String
    ) {
        val retIn = API.getRetrofitInstance().create(API::class.java)
        val register = Register(name, correo,password,address,city, country, description, lastname, postalCode.toInt(),phone)

        retIn.addUsuari(register).enqueue(
            object : retrofit2.Callback<okhttp3.ResponseBody> {
                override fun onResponse(call: Call<okhttp3.ResponseBody>, response: Response<okhttp3.ResponseBody>) {
                    if (response.code() == 201) {
                        Toast.makeText(requireContext(), "Registration success!", Toast.LENGTH_SHORT)
                            .show()
                        findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                    }
                    else{
                        Toast.makeText(requireContext(), "Registration failed!", Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                override fun onFailure(call: Call<okhttp3.ResponseBody>, t: Throwable) {
                    Toast.makeText(
                        requireContext(),
                        t.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
        )
    }

    private fun showDatePickerDialog() {
        val datePicker = DatePickerFragment { day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(childFragmentManager, "datePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int) {
        val selectedDate = day.toString() + " / " + (month + 1) + " / " + year
    }

}