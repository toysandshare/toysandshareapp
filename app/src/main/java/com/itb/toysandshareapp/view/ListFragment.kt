package com.itb.toysandshareapp.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.itb.toysandshareapp.OnClickListener
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.adapter.ProductAdapter
import com.itb.toysandshareapp.databinding.FragmentListBinding
import com.itb.toysandshareapp.model.Product
import com.itb.toysandshareapp.viewmodel.ProductsViewModel

class ListFragment : Fragment(), OnClickListener {
    lateinit var binding: FragmentListBinding
    val viewmodel : ProductsViewModel by activityViewModels()
    lateinit var productAdapter: ProductAdapter
    lateinit var linearLayoutManager: RecyclerView.LayoutManager

    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)!!

        val email = preferences.getString("email", "")
        val password = preferences.getString("password", "")
        viewmodel.connect(email!!, password!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.fabAdd.setOnClickListener{
            findNavController().navigate(R.id.action_nav_list_to_nav_add)
        }
        setUpRecyclerView(mutableListOf())
        viewmodel.products.observe(viewLifecycleOwner, Observer {
            setUpRecyclerView(it)
        })

        binding.refresh.setColorSchemeResources(R.color.brightCoral)
        binding.refresh.setProgressBackgroundColorSchemeResource(R.color.white)

        binding.refresh.setOnRefreshListener(OnRefreshListener {
            // Esto se ejecuta cada vez que se realiza el gesto
            viewmodel.products.observe(viewLifecycleOwner, Observer {
                setUpRecyclerView(it)
            })
            Handler(Looper.getMainLooper()).postDelayed({
                if (binding.refresh.isRefreshing) {
                    binding.refresh.isRefreshing = false
                }
            }, 2000)
        })
    }

    private fun setUpRecyclerView(it: MutableList<Product>) {

        productAdapter = ProductAdapter(it, this)
        linearLayoutManager = GridLayoutManager(context,2, RecyclerView.VERTICAL,false)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = productAdapter
        }
    }

    override fun onClick(product: Product) {
        viewmodel.select(product)
        findNavController().navigate(R.id.action_nav_list_to_nav_product)

    }

}