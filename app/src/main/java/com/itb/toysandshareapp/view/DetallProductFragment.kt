package com.itb.toysandshareapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.firebase.storage.FirebaseStorage
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentDetallProductBinding
import com.itb.toysandshareapp.viewmodel.ProductsViewModel
import java.io.File


class DetallProductFragment : Fragment() {
    private val model: ProductsViewModel by activityViewModels()
    lateinit var preferences: SharedPreferences

    lateinit var binding: FragmentDetallProductBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetallProductBinding.inflate(layoutInflater)
        return binding.root
    }
    @SuppressLint("SetTextI18n", "QueryPermissionsNeeded")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        preferences = activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)!!

        val email = preferences.getString("email", "")
        val password = preferences.getString("password", "")
        model.connectMyInfo(email!!, password!!)
        println("EMAIL AQUIIIIII del Detall PRODUCTS: $email : $password")
        //         Comprueba que el usuario loggeado haya introducido esos productos, para poder borrarlos.
        model.userLogged.observe(viewLifecycleOwner, Observer {
            val usuari = it
            model.selectedProduct.observe(viewLifecycleOwner, Observer {
                val product = it
                if (usuari.id == product.usuari_id){
                    binding.fabDelete.isEnabled = true
                    binding.fabDelete.isClickable = true
                    binding.buttonForEdit.isEnabled = true
                    binding.buttonForEdit.isClickable = true
                } else{
                    binding.fabDelete.isEnabled = false
                    binding.buttonForEdit.isEnabled = false
                }
                if (binding.fabDelete.isClickable){
                    binding.fabDelete.setOnClickListener {
                        model.remove(product)
                        findNavController().navigate(R.id.action_nav_product_to_nav_list)
                    }
                }
                if (!binding.fabDelete.isEnabled){
                    binding.fabDelete.isInvisible = true
                }
                if (binding.buttonForEdit.isClickable){
                    binding.buttonForEdit.setOnClickListener {
                        model.selectedProduct
                        findNavController().navigate(R.id.editProductFragment)
                    }
                }
                if (!binding.buttonForEdit.isEnabled){
                    binding.buttonForEdit.isInvisible = true
                }
                model.userInfo(product.usuari_id)
            })
        })

        model.userVendor.observe(viewLifecycleOwner, Observer {
            val usuari = it
            model.selectedProduct.observe(viewLifecycleOwner, Observer {
                val product = it
                val storage =
                    FirebaseStorage.getInstance().reference.child("images/${product.imageLink}")
                val localFile = File.createTempFile("temp", "jpeg")
                storage.getFile(localFile).addOnSuccessListener {
                    val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                    binding.imgItem.setImageBitmap(bitmap)
                }.addOnFailureListener {

                }
                binding.pName.text = product.productName
                binding.price.text = "${product.price} €"
                binding.descriptionText.text = product.productDescription
                Glide.with(requireContext())
                    .load(storage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(binding.imgItem)
                binding.imgShare.setOnClickListener {
                    val intent = Intent()
                    intent.action = Intent.ACTION_SEND
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.itb.toysandshareapp"
                    )
                    intent.type = "text/plain"
                    startActivity(Intent.createChooser(intent, "Share with..."))
                }
                binding.imgWhatsApp.setOnClickListener {
                    val msj = "Hola, me gustaría comprar o cambiar tu producto, con nombre: ${product.productName}"
                    val numeroTel = usuari.phone
                    val intent = Intent(Intent.ACTION_VIEW)
                    val uri = "whatsapp://send?phone=$numeroTel&text=$msj"
                    intent.data = Uri.parse(uri)
                    startActivity(intent)
                }
                binding.imgTelefono.setOnClickListener {
                    val sendIntent = Intent().apply {
                        action = Intent.ACTION_DIAL
                        data = Uri.parse("tel: ${usuari.phone}")
                    }
                    startActivity(sendIntent)
                }
                binding.imgLocation.setOnClickListener {
                    val gmmIntentUri =
                        Uri.parse("geo:0,0?q=${usuari.address}, ${usuari.postalCode}, ${usuari.city}, ${usuari.country}")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    startActivity(mapIntent)
                }

            })
        })
    }

}