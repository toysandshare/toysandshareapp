package com.itb.toysandshareapp.view

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.storage.FirebaseStorage
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentAddProductBinding
import com.itb.toysandshareapp.model.Product
import com.itb.toysandshareapp.viewmodel.ProductsViewModel
import java.util.*
import androidx.lifecycle.Observer


class AddProductFragment : Fragment() {
    lateinit var binding: FragmentAddProductBinding
    private val model: ProductsViewModel by activityViewModels()
    lateinit var imageUri : Uri
    lateinit var preferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAddProductBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferences = activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)!!

        val email = preferences.getString("email", "")
        val password = preferences.getString("password", "")

        model.connectMyInfo(email!!, password!!)

        binding.addImages.setOnClickListener { launchGallery() }

        model.userLogged.observe(viewLifecycleOwner, Observer {
            val usuari = it
            binding.addProducto.setOnClickListener {
                val price = binding.precio.editText?.text.toString().toDouble()
                val productDescription = binding.description.editText?.text.toString().trim()
                val productLocation = "${usuari.address}, ${usuari.city}, ${usuari.country}"
                val productName = binding.productName.editText?.text.toString().trim()
                val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
                val now = Date()
                val fileName = formatter.format(now)
                uploadImage(fileName)
                val imageLink = fileName

                val newProduct = Product( null, imageLink, price, productDescription, productLocation, productName, usuari.id)
                model.add(usuari.id, newProduct)
                findNavController().navigate(R.id.action_nav_add_to_nav_list)
            }
        })
    }

    @SuppressLint("NewApi")
    private fun uploadImage(fileName: String) {
        val storageReference = FirebaseStorage.getInstance().getReference("images/$fileName")
        storageReference.putFile(imageUri).addOnSuccessListener{
            binding.imagenProd.setImageURI(null)
            Toast.makeText(requireContext(), "SUCCESS", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Failure", Toast.LENGTH_SHORT).show()
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) {
                    imageUri = data.data!!
                    binding.imagenProd.setImageURI(imageUri)
                }
            }
        }

    private fun launchGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }
}


