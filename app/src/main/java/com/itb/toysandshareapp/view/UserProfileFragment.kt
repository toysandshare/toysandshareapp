package com.itb.toysandshareapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.itb.toysandshareapp.LoginActivity
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentUserProfileBinding
import com.itb.toysandshareapp.viewmodel.ProductsViewModel

class UserProfileFragment : Fragment() {
    lateinit var binding: FragmentUserProfileBinding
    private val model: ProductsViewModel by activityViewModels()
    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)!!

        val email = preferences.getString("email", "")
        val password = preferences.getString("password", "")
        model.connectMyInfo(email!!, password!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentUserProfileBinding.inflate(layoutInflater)
        return binding.root
    }
    @SuppressLint("SetTextI18n", "CommitPrefEdits")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mEditor = preferences.edit()
        binding.edit.setOnClickListener {
            model.userLogged.observe(viewLifecycleOwner, Observer {
                val usuari = it
                model.selectUser(usuari)
            })
            findNavController().navigate(R.id.action_nav_user_to_editUserFragment)
        }

        binding.delete.setOnClickListener {
            model.userLogged.observe(viewLifecycleOwner, Observer {
                val usuari = it
                model.removeUser(usuari)
                mEditor.remove("email")
                mEditor.remove("password")
                mEditor.clear()
                mEditor.commit()
                goToLogin()
            })
        }

        model.userLogged.observe(viewLifecycleOwner, Observer{
            val usuari = it
            val webImage = "https://toysandshare.alwaysdata.net${usuari.profileImage}"

            Glide.with(requireContext())
                .load(webImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imgProfile)
            binding.txtUsername.text = "${usuari.name} ${usuari.lastname}"
            binding.email.text = usuari.email
            binding.telefono.text = usuari.phone
            binding.description.text = usuari.description
            binding.direccion.text = "${usuari.address}, ${usuari.postalCode}, ${usuari.city}, ${usuari.country} "

        })
    }

    private fun goToLogin() {
        val intent = Intent(requireContext(), LoginActivity::class.java)
        startActivity(intent)
    }
}