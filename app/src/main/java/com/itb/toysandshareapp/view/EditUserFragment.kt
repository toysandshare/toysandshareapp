package com.itb.toysandshareapp.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.firebase.storage.FirebaseStorage
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentEditUserBinding
import com.itb.toysandshareapp.model.Usuari
import com.itb.toysandshareapp.viewmodel.ProductsViewModel
import java.text.SimpleDateFormat
import java.util.*

class EditUserFragment : Fragment() {

   private lateinit var binding: FragmentEditUserBinding
    private val viewmodel : ProductsViewModel by activityViewModels()
    lateinit var usuariEdit: Usuari
    lateinit var imageUri : Uri

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEditUserBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usuariEdit = viewmodel.userLogged.value!!
//        binding.addImages.setOnClickListener { launchGallery() }

        viewmodel.userLogged.observe(viewLifecycleOwner, Observer {
            val usuari = it

            binding.editUserName.editText?.text = usuari.name.toEditable()
            binding.Eapellido.editText?.text = usuari.lastname.toEditable()
            binding.editUserAddress.editText?.text = usuari.address.toEditable()
            binding.editUserPais.editText?.text = usuari.country.toEditable()
            binding.editUserCiudad.editText?.text = usuari.city.toEditable()
            binding.editUserCd.editText?.text = "${usuari.postalCode}".toEditable()

            usuariEdit = usuari

        })

        binding.updateUser.setOnClickListener {
                updatePhotoUser()
        }
    }

    private fun updatePhotoUser() {
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        val fileName = formatter.format(now)
        val storage = FirebaseStorage.getInstance().getReference("images/$fileName")

        usuariEdit.name = binding.editUserName.editText?.text.toString().trim()
        usuariEdit.lastname = binding.Eapellido.editText?.text.toString().trim()
        usuariEdit.address = binding.editUserAddress.editText?.text.toString().trim()
        usuariEdit.country = binding.editUserPais.editText?.text.toString().trim()
        usuariEdit.city = binding.editUserCiudad.editText?.text.toString().trim()
        usuariEdit.postalCode = binding.editUserCd.editText?.text.toString().toInt()
//        usuariEdit.profileImage = fileName
        viewmodel.updateUser(usuariEdit)
        findNavController().navigate(R.id.action_nav_editUser_to_nav_list)

    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) {
                    imageUri = data.data!!
//                    binding.editProductImagePrev.setImageURI(imageUri)
                }
            }
        }

    @SuppressLint("NewApi")
    private fun uploadImage(fileName: String) {
        val storageReference = FirebaseStorage.getInstance().getReference("images/$fileName")
        storageReference.putFile(imageUri).addOnSuccessListener{
//            binding.editProductImagePrev.setImageURI(null)
            Toast.makeText(requireContext(), "SUCCESS", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Failure", Toast.LENGTH_SHORT).show()
        }
    }


    private fun launchGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }



    private fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

}