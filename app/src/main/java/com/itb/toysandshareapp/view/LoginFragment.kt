package com.itb.toysandshareapp.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.itb.toysandshareapp.MainActivity
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.FragmentLoginBinding
import com.itb.toysandshareapp.retrofit.API
import com.itb.toysandshareapp.viewmodel.ProductsViewModel

class LoginFragment : Fragment(), View.OnClickListener {
    private lateinit var viewModel : ProductsViewModel
    private lateinit var binding: FragmentLoginBinding
    lateinit var  sharedPreferences : SharedPreferences
    var isRemembered = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)!!
        isRemembered = sharedPreferences.getBoolean("CHECKBOX", false)
        if (isRemembered) {
            val intent = Intent(requireContext(), MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.login.setOnClickListener {
            val email = binding.username.editText?.text.toString().trim()
            val password = binding.password.editText?.text.toString().trim()
            val checkBox = binding.checkbox.isChecked

            if (email.isEmpty()){
                binding.username.error = "Campo obligatorio"
                binding.username.requestFocus()
                return@setOnClickListener
            }

            if (password.isEmpty()){
                binding.password.error = "Campo obligatorio"
                binding.password.requestFocus()
                return@setOnClickListener
            }

            API.create(email, password)
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("email", email)
            editor.putString("password", password)
            editor.putBoolean("CHECKBOX", checkBox)
            editor.apply()
            if(checkBox){
                Toast.makeText(requireContext(), "Information Saved!!", Toast.LENGTH_LONG).show()
            }
            goToHome(email, password)
        }

        binding.register.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun goToHome(email: String, password: String) {
        val intent = Intent(requireContext(), MainActivity::class.java)
        startActivity(intent)
    }

    override fun onClick(p0: View?) {
        TODO("Not yet implemented")
    }

}