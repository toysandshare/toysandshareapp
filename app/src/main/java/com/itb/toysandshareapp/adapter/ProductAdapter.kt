package com.itb.toysandshareapp.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.firebase.storage.FirebaseStorage
import com.itb.toysandshareapp.OnClickListener
import com.itb.toysandshareapp.R
import com.itb.toysandshareapp.databinding.ProductViewBinding
import com.itb.toysandshareapp.model.Product
import java.io.File

class ProductAdapter(private var products: MutableList<Product>, private var listener: OnClickListener): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.product_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductAdapter.ViewHolder, position: Int) {
        val product = products[position]

        with(holder){
            val storage = FirebaseStorage.getInstance().reference.child("images/${product.imageLink}")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.productImage.setImageBitmap(bitmap)
            }.addOnFailureListener{

            }
            setListener(product)
            Glide.with(context)
                .load(storage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.productImage)
            binding.nomProducte.text = product.productName
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = ProductViewBinding.bind(view)

        fun setListener(product: Product){
            binding.root.setOnClickListener {
                listener.onClick(product)
            }
        }
    }

    fun delete(product: Product) {
        val index = products.indexOf(product)
        if(index != -1){
            products.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun add(product : Product) {
        products.add(product)
        notifyItemInserted(products.size-1)
    }

}

